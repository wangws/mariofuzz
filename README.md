# mariofuzz

## How it works

### 0.config.json

`static_output`:"./static_analysis/output"

`source`:"<com.example.fortestapp.MainActivity: java.lang.String getText(java.lang.String)>"

`sink`:"<com.example.fortestapp.StringReplace: java.lang.String shortenString(java.lang.String)>"

`app_name`:"fortest"

`fuzz_out`:"fuzz_out"

`target_process`:"com.example.fortestapp"

`frida_command`:"python3 ./frida/example.py"

`path_generation_command`:"python3 ./frida/ path_generation.py"

`frida_out_path`:"./frida/frida_out"

`frida_js`:"./frida/example.js"

`payload`:"hello world"

`adb`:"adb shell input touchscreen tap 566 1254"

`adb_start_command`:"adb shell am start -n "com.example.fortestapp/com.example.fortestapp.MainActivity\" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER"

`adb_push_dir`:"/data/test.txt"

`adb_over`:"adb shell am force-stop com.example.fortestapp"


### 1.static analysis

#### 1.1 Use flowdroid to get path from source to sink and get call graph
In static_analysis directory:
```shell
java -cp flowdroid_analysis.jar StaticAnalysis config_file/android.jar input/fortest.apk config_file/SourcesAndSinks_.txt config_file/AndroidCallbacks.txt output/
```
parameters:
arg 0:android.jar
arg 1:apk
arg 2:sources and sinks
arg 3:callbacks
arg 4:output path

#### 1.2 Construct call graph with Networkx[not use now]
In static_analysis directory:
```
python3 cg_analysis.py ../config.json
```
parameters:
arg 0:config file path

This step's result is the same as flowdroid taint analysis.So it is left to be use for future.

### 2.run brige script

#### 2.1 brige.py
In the parent directory:

AFL will produced a file after get a new input file by mutating.This script is used to monitor afl's output file then execute frida to get trace.

#### 2.2 brige2.py

This script monitor that the brige.py’s output file then start the test APP with given tesecase.At the same time it will produce 2 files: paths and completeness. The former shows the trace bits which the paths cover, the latter shows the payload completeness.

### 3.afl

If AFL get a new tesecase by mutating, it will push the testecase to Android then output a file.The script brige.py monitor the file is produced then start up the frida program.

## How to use

First of all, start the frida service in you android phone.

Execute the following commands in order:
In parent directory:

```
python3 brige2.py config.json 
```
In parent directory:
```
python3 brige.py config.json
```
In afl-fuzz directory:
``` 
./afl-fuzz -a ../../config.json -i testcases/images/png/ -o ../fuzz_out AndroidFuzz @@
```
