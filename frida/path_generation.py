#-*- coding=utf-8 -*-
import os,sys
import time
import codecs
import random
import json

last_coverage_file_time = ""
global basic_block2random

paths = {}
lost = set()
def main(coverage_file):
    global last_coverage_file_time,paths,basic_block2random
    cur = time.ctime(os.stat(coverage_file).st_mtime)
    if last_coverage_file_time=="" or last_coverage_file_time!=cur:

        f = open(coverage_file)
        line = f.readline()
        pre = 0
        while line:
            key = int(line,16)
            if key not in basic_block2random:
                basic_block2random[int(line,16)]=random.randint(0,65535)
            cur_key = (pre>>1)^basic_block2random[key]
            pre = basic_block2random[key]
            if cur_key not in paths:
                paths[cur_key]=1
            else:
                paths[cur_key]=paths[cur_key]+1
            line = f.readline()
        f.close()
        last_coverage_file_time = cur

if __name__ == "__main__":
    global basic_block2random,data

    config_path = sys.argv[1]
    print(config_path)
    f = open(config_path,"r")
    data = json.load(f)


    # basic_block2random_path = "/home/wws/Pictures/MaFuzz/mariofuzz/brige_out/basic_block2random"
    basic_block2random_path = data["frida_out_path"]+os.sep+data["app_name"]+"_basic_block2random"
    if os.path.exists(basic_block2random_path):
        basic_block2random = json.load(open(basic_block2random_path,"r"))
    else:
        basic_block2random = {}

    coverage_file = data["frida_out_path"]+os.sep+data["app_name"]+"_firda_out.txt"
    paths_out = data["frida_out_path"]+os.sep+data["app_name"]+"_paths.json"
    # while True:
    main(coverage_file)

    f=open(basic_block2random_path,"w")
    json.dump(basic_block2random,f)
    f.close()
    f=open(paths_out,"w")
    json.dump(paths,f)
    f.close()
    print("path json finished.");
    ff=open(paths_out.replace(".json",""),"w")
    ff.write("path finished.\n")
    ff.close()
