# -*- coding: UTF-8 -*-

import sys
import frida
import time
import difflib
import json
import os
# js_file_name = "/home/wws/Pictures/MaFuzz/mariofuzz/frida/example.js"
# origin_payload = "hello world"
js_file_name = ""
origin_payload = ""

function_id = 0
global data,fuzz_out


base ='''
function %s()
{
  Java.perform(function () {
    //console.log("hello")
    var MainActivity = Java.use("%s");
        //console.log("world")
        MainActivity.%s.implementation=function(a){
            console.log(arguments[0]);
            var ret = this.%s(a);
            send("ret:"+ret)
            return ret;
            // return "hello";
        }
    });
}
%s();
'''

last_base ='''
function %s()
{
  Java.perform(function () {
    //console.log("hello")
    var MainActivity = Java.use("%s");
        //console.log("world")
        MainActivity.%s.implementation=function(a){
            console.log(arguments[0]);
            var ret = this.%s(a);
            send("last ret:"+ret)
            return ret;
            // return "hello";
        }
    });
}
%s();
'''

def get_similar_two_strings(s1,s2):
    return difflib.SequenceMatcher(None, s1, s2).quick_ratio()

def get_js_code():
    global function_id
    code = ""
    package2method,last_package_method = get_to_processed_functions()
    # for key in package2method:
    for key in package2method:
        for value in package2method[key]:
            new_base = base%("function"+str(function_id),key,value,value,"function"+str(function_id))
            if key==last_package_method[0] and value==last_package_method[1]:
                new_base = last_base%("function"+str(function_id),key,value,value,"function"+str(function_id))
            # else:
            function_id = function_id+1
            code = code+new_base
    # print(code)
    js_file = open(js_file_name, 'r',encoding='utf-8')
    # print(js_file.read())
    # print(code+js_file.read())
    return code+js_file.read()
def get_package_function(s):
        # s = "$r5 = virtualinvoke $r4.<com.example.fortestapp.MainActivity: java.lang.String getText(java.lang.String)>(\"/data/test.txt\")"
    x = s.index("<")
    y = s.index(">")
    sub=s[x+1:y]
    leftright = sub.split(":")
    left = leftright[0]
    right = leftright[1]
    right = right.strip()

    package = left
    function = right[right.index(" ")+1:right.index("(")]

    # print(package)
    # print(function)	
    return package,function
def main(target_process):
    global data,fuzz_out
    # session = frida.get_usb_device().attach(target_process)
    # session = frida.get_usb_device().spawn(target_process)
    # print(dir(session))
    device = frida.get_usb_device()
    pid = device.spawn([target_process])
    device.resume(pid)
    time.sleep(1) #Without it Java.
    session = device.attach(pid)
    script = session.create_script(get_js_code())
    # path_frida_out = "/home/wws/Pictures/MaFuzz/mariofuzz/brige_out/frida_out.txt"
    path_frida_out = data["frida_out_path"]+os.sep+data["app_name"]+"_firda_out.txt"
    # path_completeness = "/home/wws/Pictures/MaFuzz/mariofuzz/brige_out/payload_completeness.txt"
    path_completeness = data["frida_out_path"]+os.sep+data["app_name"]+"_payload_completeness"
    last_method_completeness = data["frida_out_path"]+os.sep+data["app_name"]+"_lastmethod_payload_completeness"
    f=open(path_frida_out,"w")
    f.close()
    f=open(path_completeness,"w")
    f.close()
    def on_message(message, data):
        
        origin_payload_path = open(os.path.join(fuzz_out,".cur_input"),"r",encoding="utf8", errors='ignore')
        origin_payload = "".join(origin_payload_path.readlines())
        origin_payload_path.close()
        # print()
        # script.post("ppp")
        # print("########################################")
        # print("[%s] -> %s" % (message, data))

        
            # content=message["payload"]
        # if message["type"]=="send":
                # content = message["payload"]
        # path_frida_out = "/home/wws/Pictures/MaFuzz/mariofuzz/brige_out/frida_out.txt"
        # path_completeness = "/home/wws/Pictures/MaFuzz/mariofuzz/brige_out/payload_completeness.txt"
        # print(type(message["payload"]))

        if message["type"]=='send':
            payload = message["payload"]
            # print(payload)
            # print(type(type(payload)))
            if type(payload)==type(""):
                    # print(payload)
                    if "----------" in payload:
                        # print(payload)
                        pass
                    elif "ret:" in payload:
                        print("#################################################")
                        payload = payload.replace("\n","")
                        origin_payload = origin_payload.replace("\n","")
                        print("payload:"+payload)
                        print("origin payload:"+origin_payload)
                        print("#################################################")
                        completeness = get_similar_two_strings(origin_payload,payload.replace("ret:",""))
                        print("completeness:"+str(completeness))
                        f = open(path_completeness,"a")
                        f.write(str(completeness)+"\n")
                        f.close()

                        if "last ret:" in payload:
                            f = open(last_method_completeness,"a")
                            f.write(str(completeness)+"\n")
                            f.close()
            elif type(payload)==type([]):
                    # pass
                    f = open(path_frida_out,"a")
                    # print(payload)
                    # print(type(payload[2]))
                    # print("payload2:"+payload[2])
                    l = len(payload)
                    for m in range(l):
                        f.write(payload[m][1]+"\n")
                    f.close()
        # print(message+"->"+data)
        # return
        # print("########################################")
        # pass
    script.on('message', on_message)
    script.load()
    input()
    session.detach()
def get_to_processed_functions():
    # path = "/home/wws/Pictures/MaFuzz/mariofuzz/brige_out/sootOutput/fortest_path"
    path = data["static_output"]+os.sep+data["app_name"]+"_path"
    f = open(path,"r")
    line=f.readline()
    from collections import defaultdict
    package2method = defaultdict(set)
    last_package_method = ["",""]
    while line:
        if "<" not in line:
            line = f.readline()
            continue
        # print(line)
        package,function = get_package_function(line)
        # print(package)
        # print(function)
        package2method[package].add(function)
        last_package_method[0]=package
        last_package_method[1]=function
        line = f.readline()
    return package2method,last_package_method
if __name__ == '__main__':
    global data,fuzz_out
    # device = frida.get_usb_device()
    # pid = device.spawn(["com.example.a11x256.frida_test"])
    # device.resume(pid)
    # time.sleep(1)  # Without it Java.perform silently fails
    # session = device.attach(pid)
    config_path = sys.argv[1]
    print(config_path)
    f = open(config_path,"r")
    data = json.load(f)

    js_file_name = data["frida_js"]
    origin_payload = data["payload"]
    fuzz_out = data["fuzz_out"]

    # target_process = "com.example.fortestapp"
    target_process = data["target_process"]
    main(target_process)


