
function StalkerExeample() 
{
	var threadIds = [];
	Process.enumerateThreads({
		onMatch: function (thread) 
		{
			threadIds.push(thread.id);
			// console.log("Thread ID: " + thread.id.toString()+" "+thread);
		},
		onComplete: function () 
		{

			threadIds.forEach(function (threadId) 
				{
					Stalker.follow(threadId, 
					{
						events: {
              block:true,
              compile:true
            },
          onReceive: function (events) { 
            var message = Stalker.parse(events, { 
              annotate: true, // to display the type of event 
              stringify: true // 
            }); 
            // console.log(message)
            // console.log("------------------------")
            send(message); 
            send("------------------"); 
            }
				});
			});
    }
	});
}
StalkerExeample();