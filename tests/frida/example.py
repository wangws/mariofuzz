# -*- coding: UTF-8 -*-

import sys
import frida
js_file_name = "/home/wws/Pictures/MaFuzz/mariofuzz/tests/frida/example.js"
def on_message(message, data):
	# print()
	print("[%s] -> %s" % (message, data))

def get_js_code():
    js_file = open(js_file_name, 'r',encoding='utf-8')
    # print(js_file.read())
    return js_file.read()

def main(target_process):
	session = frida.get_usb_device().attach(target_process)
	# session = frida.get_usb_device().spawn(target_process)
	# print(dir(session))
	script = session.create_script(get_js_code())
	script.on('message', on_message)
	script.load()
	input()
	session.detach()

if __name__ == '__main__':
    target_process = "com.example.fortestapp"
    main(target_process)