Java.perform(function () {
  // Function to hook is defined here
  var MainActivity = Java.use('com.example.fortestapp.MainActivity');

  // Whenever button is clicked
  // var onClick = MainActivity.onClick;
  MainActivity.onClick.implementation = function (v) {
    // Show a message to know that the function got called
    send('Intercepted before button click event. Running onClick code...');

    // Call the original onClick handler
    onClick.call(this, v);

    // Log to the console that it's done, and we should have the flag!
    console.log('Done:' + JSON.stringify(this));
  };
  // Whenever button is clicked
  // console.log(MainActivity);
  // var onClick = MainActivity.onClick;

  // console.log(onClick);
  // onClick.performClick.call();
    // lastActivity = Java.retain(this);
    // this.onResume();
  // };
  // onClick.performClick
  // onClick.implementation = function (v) {
  //   // Show a message to know that the function got called
  //   send('Intercepted before button click event. Running onClick code...');

  //   // Call the original onClick handler
  //   onClick.call(this, v);

  //   // Log to the console that it's done, and we should have the flag!
  //   console.log('Done:' + JSON.stringify(this));
  // };
});