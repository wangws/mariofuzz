import os,sys
import pyinotify
from pyinotify import WatchManager, Notifier, ProcessEvent, IN_DELETE, IN_CREATE, IN_MODIFY
import subprocess
import datetime
import time
import json

# fuzz_out = ""

afl2brige = "afl2brige.txt"
global data
global config_path
global fuzz_out
global app_paths_finished_path
global app_paths_finished_path_time
def execute_command(cmdstring):   
    global app_paths_finished_path,app_paths_finished_path_time 
    sub = subprocess.Popen(cmdstring, stdin=subprocess.PIPE, bufsize=40960, shell=True)
    while True:
        if sub.poll() is not None:
            break
        time.sleep(0.1)
        # os.stat("")st_mtime
        cur_time = 0
        if os.path.exists(app_paths_finished_path):
            cur_time = os.stat(app_paths_finished_path).st_mtime
        if cur_time!=app_paths_finished_path_time:
            print("-------------------------------")
            print(cur_time)
            print(app_paths_finished_path_time)
            print("------------------------------")
            app_paths_finished_path_time = cur_time
            sub.kill()
            break


def frida_start():
    global data,config_path
    print("brige start")
    # command = "python3 ./frida/example.py"
    command = data["frida_command"]+" "+os.path.abspath(config_path)
    print(command)
    execute_command(command)
    # command = "python3 ./frida/path_generation.py"
    # command = data["path_generation_command"]+" "+os.path.abspath(config_path)
    # print(command)
    # execute_command(command,10)
    print("brige over")

class EventHandler(ProcessEvent):
    def process_IN_CREATE(self, event):
        # print("Create file: % s" % os.path.join(event.path, event.name))
        if afl2brige in event.name:
            print("Create file: % s" % os.path.join(event.path, event.name))
            frida_start()
    def process_IN_DELETE(self, event):
        # print("hello world")
        # print("Deletefile: % s" % os.path.join(event.path, event.name))
        if afl2brige in event.name:
            print("Deletefile: % s" % os.path.join(event.path, event.name))
            frida_start()
    def process_IN_MODIFY(self, event):
        # print("Modifyfile: % s" % os.path.join(event.path, event.name))
        if afl2brige in event.name:
            print("Modifyfile: % s" % os.path.join(event.path, event.name))
            frida_start()
def main():
    # path = os.path.join(fuzz_out,"command.txt")
    path1 = fuzz_out
    # path2 = frida_out
    wm = WatchManager()
    mask = IN_DELETE | IN_CREATE | IN_MODIFY
    notifier = Notifier(wm, EventHandler())
    wm.add_watch(path1, mask, auto_add=True, rec=True)
    # wm.add_watch(path2, mask, auto_add=True, rec=True)
    print('now starting monitor % s' % (path1))
    while True:
        try:
            notifier.process_events()
            if notifier.check_events():
                notifier.read_events()
        except KeyboardInterrupt:
            notifier.stop()
            break

if __name__ == '__main__':
    global data,config_path,fuzz_out,app_paths_finished_path,app_paths_finished_path_time
    config_path = sys.argv[1]
    print(config_path)
    f = open(config_path,"r")
    data = json.load(f)
    fuzz_out = data["fuzz_out"]
    app_paths_finished_path = data["frida_out_path"]+os.sep+data["app_name"]+"_paths"
    if not os.path.exists(app_paths_finished_path):
        app_paths_finished_path_time = 0
    else:
        app_paths_finished_path_time = os.stat(app_paths_finished_path).st_mtime
    # frida_out = sys.argv[2]
    # fuzz_out = "fuzz_out"
    main()
    