import os,sys
import pyinotify
from pyinotify import WatchManager, Notifier, ProcessEvent, IN_DELETE, IN_CREATE, IN_MODIFY
import subprocess
import datetime
import time
from time import sleep
import json

global fuzz_out
global config_path
global data
global frida_out_file
global frida_out_dir
# path_frida_out = data["frida_out_path"]+os.sep+data["app_name"]+"_firda_out.txt"

# The below function is referenced from https://blog.csdn.net/lifestxx/java/article/details/82898751
def execute_command(cmdstring, timeout=None):    
    if timeout:
        end_time = datetime.datetime.now() + datetime.timedelta(seconds=timeout)
    sub = subprocess.Popen(cmdstring, stdin=subprocess.PIPE, bufsize=4096, shell=True)
    while True:
        if sub.poll() is not None:
            break
        time.sleep(0.1)
        if timeout:
            if end_time <= datetime.datetime.now():
                sub.kill()
                # return "TIME_OUT"

def adb_start():
    global data,config_path
    # system("adb shell input touchscreen tap 566 1254");//Nexus 6p
    # system("adb shell input touchscreen tap 419 1088");//Pixel 3a
    print("brige2 start")
    sleep(1)
    command = data["adb"]
    execute_command(command,10) #click the button
    command = data["path_generation_command"]+" "+os.path.abspath(config_path)
    # command = "python3 ./frida/path_generation.py /home/wws/Pictures/MaFuzz/mariofuzz/brige_out/frida_out.txt"
    execute_command(command,10)
    print("brige2 over")

class EventHandler(ProcessEvent):
    global frida_out_file
    def process_IN_CREATE(self, event):
        # print("Create file: % s" % os.path.join(event.path, event.name))
        if frida_out_file in event.name:
            print("Create file: % s" % os.path.join(event.path, event.name))
            adb_start()
    def process_IN_DELETE(self, event):
        # print("hello world")
        # print("Deletefile: % s" % os.path.join(event.path, event.name))
        if frida_out_file in event.name:
            print("Deletefile: % s" % os.path.join(event.path, event.name))
            adb_start()
    def process_IN_MODIFY(self, event):
        # print("Modifyfile: % s" % os.path.join(event.path, event.name))
        if frida_out_file in event.name:
            print("Modifyfile: % s" % os.path.join(event.path, event.name))
            adb_start()

def main():
    global frida_out_dir
    # path = os.path.join(fuzz_out,"command.txt")
    path1 = frida_out_dir
    # path2 = frida_out
    wm = WatchManager()
    mask = IN_DELETE | IN_CREATE | IN_MODIFY
    notifier = Notifier(wm, EventHandler())
    wm.add_watch(path1, mask, auto_add=True, rec=True)
    # wm.add_watch(path2, mask, auto_add=True, rec=True)
    print('now starting monitor % s' % (path1))
    while True:
        try:
            notifier.process_events()
            if notifier.check_events():
                notifier.read_events()
        except KeyboardInterrupt:
            notifier.stop()
            break

if __name__ == '__main__':
    global data,config_path,frida_out_file
    config_path = sys.argv[1]
    print(config_path)
    f = open(config_path,"r")
    data = json.load(f)


    fuzz_out = data["fuzz_out"]
    frida_out_file = data["app_name"]+"_firda_out.txt"
    frida_out_dir = data["frida_out_path"]

    main()
    