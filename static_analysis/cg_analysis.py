import networkx as nx
import matplotlib.pyplot as plt

import sys
import json

# path = "/home/wws/Pictures/MaFuzz/mariofuzz/flowdroid/flowdroidtest/sootOutput/application"
cg = nx.DiGraph()

# source = "<com.example.fortestapp.MainActivity: java.lang.String getText(java.lang.String)>"
# sink = "<com.example.fortestapp.StringReplace: java.lang.String shortenString(java.lang.String)>"

def main(source,sink,path):
    # global path
    cg_file = open(path,"r")

    line = cg_file.readline()

    while line:
        # print(line)
        line = line.replace("\n","")
        edge = line.split("-->")
        edge = [e.strip() for e in edge]
        # print(edge)
        # return
        cg.add_edge(edge[0],edge[1])
        cg.add_edge(edge[1],edge[0])
        line = cg_file.readline()
    for node in cg.nodes():
        if "shorten" in node:
            print(node==sink)
            print(node)
            print(sink)
    trace=nx.dijkstra_path(cg, source=source, target=sink)
    print(trace)

    
    # print(line)

if __name__ == '__main__':
    config_path = sys.argv[1]
    print(config_path)
    f = open(config_path,"r")
    data = json.load(f)
    source=data["source"]
    sink=data["sink"]
    main(source,sink)
    